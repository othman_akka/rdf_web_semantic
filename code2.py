import rdflib
import re
import numpy as np
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.decomposition import TruncatedSVD
from sklearn.preprocessing import StandardScaler
from nltk.corpus import stopwords
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def createData(dataRDF):
    graph = rdflib.Graph()
    graph.parse(dataRDF)
    
    objects = []
    predicates=[]
    subjects=[]

    for subj, pred, obj in graph:
        if re.search(r'[\u0600-\u06FF]+', obj):
            subjects.append(subj)
            predicates.append(pred)
            objects.append(obj)
    #create csv file   
    df = pd.DataFrame({"subjects" : subjects,"objects" : objects, "predictes" : predicates})
    df.to_csv("data.csv", sep=',',index=False,encoding='utf-8-sig')
    print('______csv file ceated______')
    
def cleanData(data):
    data2=[]
    for text in data:
        #we get jsut arabic object
        text=re.sub(r'[A-Za-z0-9]','',text)
        data2.append(re.sub(r'[^\w\s]','',text))
    return data

def clusteringRDF(dataCSV):
    data=pd.read_csv(dataCSV,sep=',').to_numpy()
    subjects=data[:,0]
    objects=data[:,1]
    predicates=data[:,2]
    
    dataOP=np.append(objects,predicates)
    #clean data
    data2=cleanData(dataOP)

    stopwordsA = set(stopwords.words('arabic'))
    
    #Convert a collection of raw documents to a matrix of TF-IDF features 
    #The stop_words_ attribute can get large and increase the model size when pickling.
    #so we remove stopwords from data
    #TfidfVectorizer: percentage of the words in a sentence (kind of)
    tfidf=TfidfVectorizer(stop_words=stopwordsA) 
    dataN = tfidf.fit_transform(data2).toarray()
    
    #display features
    table=pd.DataFrame(dataN, columns=tfidf.get_feature_names())
    print(table)
    
    #This transformer performs linear dimensionality reduction by means 
    #of truncated singular value decomposition 
    svd = TruncatedSVD(n_components=3, random_state = 0)
    dataOPT = svd.fit_transform(dataN)
    
    #Standardize features by removing the mean and scaling to unit variance
    #The standard score of a sample x is calculated as:
    #z = (x - u) / s  with s is s the standard deviation of the training samples or one if with_std=False
    #u is the mean of the training samples or zero if with_mean=False
    scaler = StandardScaler(copy=True, with_mean=True, with_std=True)
    dataOPS = scaler.fit_transform(dataOPT)
    
    #create clustering K_means
    kmeans = KMeans(n_clusters=3).fit(dataOPS)
    preds = kmeans.fit_predict(dataOPS)
    
    print('hhhh',len(dataOPS[:, 0]))
    print('hhhh',len(dataOPS[:, 1]))
    
    plt.scatter(dataOPS[:, 0], dataOPS[:, 1], c=preds, s=50, cmap='viridis')

    centers = kmeans.cluster_centers_
    #plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5);
    
    # Visualising the clusters
#    plt.scatter(df['x'], df['y'], c= kmeans.labels_.astype(float), s=50, alpha=0.5)
#    plt.scatter(centroids[:, 0], centroids[:, 1], c='red', s=50)
    
#    # Visualising the clusters
#    plt.scatter(dataOPS[preds == 0, 0], dataOPS[preds == 0,1],s=50,c='red',label='class1')
#    plt.scatter(dataOPS[preds == 1, 0], dataOPS[preds == 1,1],s=50,c='blue',label='class2')
#    plt.scatter(dataOPS[preds == 2, 0],dataOPS[preds == 2,1],s=50,c='green',label='class3')
#    plt.title('Clusters of rdf data')
#    plt.legend()
#    plt.show()
      
if __name__=='__main__':
    #createData("Cristiano_Ronaldo.rdf")
    clusteringRDF("data.csv")
    
    





